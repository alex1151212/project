﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

namespace Darts
{
    public class PlayerManager : MonoBehaviourPunCallbacks, IPunObservable
    {

        public bool enable;

        public int Round;
        public int dart;
        public bool ismyturn;

        public bool Catch;

        public int Score;

        private static Trigger trigger;

        public static Arrow arrow;

        // public bool is_winner;

        [Tooltip("玩家角色的 instance")]
        public static GameObject LocalPlayerInstance;

        public void Init()
        {

            if (arrow == null)
                arrow = Instantiate<Arrow>(Resources.Load<Arrow>("Prefabs/dart"));

            Catch = false;
            trigger = null;
            enable = true;
            arrow.Init();
        }
        public void Catched()
        {

            Catch = true;

        }

        public static void SetTrigger(Trigger tri)
        {
            trigger = tri;
        }

        private void OnGUI()
        {
            if (Score != -1)
            {
                GUI.Label(new Rect(400, 80, 100, 30), "Your Score is " + Score);

                GUI.Label(new Rect(400, 100, 150, 30), "push blank to rebegin");
            }

        }
        private void Awake()
        {
            // 記錄玩家角色的 instance, 避免在重載場景時, 又再生成一次
            if (photonView.IsMine)
            {
                PlayerManager.LocalPlayerInstance = this.gameObject;
            }

            // 標註玩家角色的 instance 不會在重載場景時被砍殺掉
            DontDestroyOnLoad(this.gameObject);
        }
        void Start()
        {
            Init();
            Score = 301;
            enable = true;
        }

        void Update()
        {

            // GameManager.Instance.LeaveRoom();
            // is_winner = true;

            if (Input.GetKeyDown(KeyCode.Space))
            {
                Debug.Log("JUMP");
                Init();
            }

            if (Catch == true)
            {
                if (enable)
                {
                    if (arrow.transform.position.z - this.transform.position.z > 0.05)
                    {

                        arrow.GetComponent<Rigidbody>().isKinematic = true;

                        arrow.GetComponent<Rigidbody>().useGravity = false;

                        print(trigger.gameObject.name);

                        switch (trigger.gameObject.name)
                        {
                            case "20":
                                if (trigger.gameObject.tag == "triple")
                                {
                                    Score -= 60;
                                }
                                else if (trigger.gameObject.tag == "double")
                                {
                                    Score -= 40;
                                }
                                else if (trigger.gameObject.tag == "point")
                                {
                                    Score -= 20;
                                }

                                break;

                            case "19":
                                if (trigger.gameObject.tag == "triple")
                                {
                                    Score -= 57;
                                }
                                else if (trigger.gameObject.tag == "double")
                                {
                                    Score -= 38;
                                }
                                else if (trigger.gameObject.tag == "point")
                                {
                                    Score -= 19;
                                }

                                break;

                            case "18":
                                if (trigger.gameObject.tag == "triple")
                                {
                                    Score -= 54;
                                }
                                else if (trigger.gameObject.tag == "double")
                                {
                                    Score -= 36;
                                }
                                else if (trigger.gameObject.tag == "point")
                                {
                                    Score -= 18;
                                }

                                break;

                            case "17":
                                if (trigger.gameObject.tag == "triple")
                                {
                                    Score -= 51;
                                }
                                else if (trigger.gameObject.tag == "double")
                                {
                                    Score -= 34;
                                }
                                else if (trigger.gameObject.tag == "point")
                                {
                                    Score -= 17;
                                }

                                break;

                            case "16":
                                if (trigger.gameObject.tag == "triple")
                                {
                                    Score -= 48;
                                }
                                else if (trigger.gameObject.tag == "double")
                                {
                                    Score -= 32;
                                }
                                else if (trigger.gameObject.tag == "point")
                                {
                                    Score -= 16;
                                }

                                break;

                            case "15":
                                if (trigger.gameObject.tag == "triple")
                                {
                                    Score -= 45;
                                }
                                else if (trigger.gameObject.tag == "double")
                                {
                                    Score -= 30;
                                }
                                else if (trigger.gameObject.tag == "point")
                                {
                                    Score -= 15;
                                }

                                break;

                            case "14":
                                if (trigger.gameObject.tag == "triple")
                                {
                                    Score -= 42;
                                }
                                else if (trigger.gameObject.tag == "double")
                                {
                                    Score -= 28;
                                }
                                else if (trigger.gameObject.tag == "point")
                                {
                                    Score -= 14;
                                }

                                break;

                            case "13":
                                if (trigger.gameObject.tag == "triple")
                                {
                                    Score -= 39;
                                }
                                else if (trigger.gameObject.tag == "double")
                                {
                                    Score -= 26;
                                }
                                else if (trigger.gameObject.tag == "point")
                                {
                                    Score -= 13;
                                }

                                break;

                            case "12":
                                if (trigger.gameObject.tag == "triple")
                                {
                                    Score -= 36;
                                }
                                else if (trigger.gameObject.tag == "double")
                                {
                                    Score -= 24;
                                }
                                else if (trigger.gameObject.tag == "point")
                                {
                                    Score -= 12;
                                }

                                break;

                            case "11":
                                if (trigger.gameObject.tag == "triple")
                                {
                                    Score -= 33;
                                }
                                else if (trigger.gameObject.tag == "double")
                                {
                                    Score -= 22;
                                }
                                else if (trigger.gameObject.tag == "point")
                                {
                                    Score -= 11;
                                }

                                break;

                            case "10":
                                if (trigger.gameObject.tag == "triple")
                                {
                                    Score -= 30;
                                }
                                else if (trigger.gameObject.tag == "double")
                                {
                                    Score -= 20;
                                }
                                else if (trigger.gameObject.tag == "point")
                                {
                                    Score -= 10;
                                }

                                break;

                            case "9":
                                if (trigger.gameObject.tag == "triple")
                                {
                                    Score -= 27;
                                }
                                else if (trigger.gameObject.tag == "double")
                                {
                                    Score -= 18;
                                }
                                else if (trigger.gameObject.tag == "point")
                                {
                                    Score -= 9;
                                }

                                break;

                            case "8":
                                if (trigger.gameObject.tag == "triple")
                                {
                                    Score -= 24;
                                }
                                else if (trigger.gameObject.tag == "double")
                                {
                                    Score -= 16;
                                }
                                else if (trigger.gameObject.tag == "point")
                                {
                                    Score -= 8;
                                }

                                break;

                            case "7":
                                if (trigger.gameObject.tag == "triple")
                                {
                                    Score -= 21;
                                }
                                else if (trigger.gameObject.tag == "double")
                                {
                                    Score -= 14;
                                }
                                else if (trigger.gameObject.tag == "point")
                                {
                                    Score -= 7;
                                }

                                break;

                            case "6":
                                if (trigger.gameObject.tag == "triple")
                                {
                                    Score -= 18;
                                }
                                else if (trigger.gameObject.tag == "double")
                                {
                                    Score -= 12;
                                }
                                else if (trigger.gameObject.tag == "point")
                                {
                                    Score -= 6;
                                }

                                break;

                            case "5":
                                if (trigger.gameObject.tag == "triple")
                                {
                                    Score -= 15;
                                }
                                else if (trigger.gameObject.tag == "double")
                                {
                                    Score -= 10;
                                }
                                else if (trigger.gameObject.tag == "point")
                                {
                                    Score -= 5;
                                }

                                break;

                            case "4":
                                if (trigger.gameObject.tag == "triple")
                                {
                                    Score -= 12;
                                }
                                else if (trigger.gameObject.tag == "double")
                                {
                                    Score -= 8;
                                }
                                else if (trigger.gameObject.tag == "point")
                                {
                                    Score -= 4;
                                }

                                break;

                            case "3":
                                if (trigger.gameObject.tag == "triple")
                                {
                                    Score -= 9;
                                }
                                else if (trigger.gameObject.tag == "double")
                                {
                                    Score -= 6;
                                }
                                else if (trigger.gameObject.tag == "point")
                                {
                                    Score -= 3;
                                }

                                break;

                            case "2":
                                if (trigger.gameObject.tag == "triple")
                                {
                                    Score -= 6;
                                }
                                else if (trigger.gameObject.tag == "double")
                                {
                                    Score -= 4;
                                }
                                else if (trigger.gameObject.tag == "point")
                                {
                                    Score -= 2;
                                }

                                break;

                            case "1":
                                if (trigger.gameObject.tag == "triple")
                                {
                                    Score -= 3;
                                }
                                else if (trigger.gameObject.tag == "double")
                                {
                                    Score -= 2;
                                }
                                else if (trigger.gameObject.tag == "point")
                                {
                                    Score -= 1;
                                }

                                break;
                        }


                        enable = false;
                    }
                }



            }

        }

        public void OnPhotonSerializeView(PhotonStream stream,
    PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                // 為玩家本人的狀態, 將 IsFiring 的狀態更新給其他玩家
                // stream.SendNext(IsFiring);
                stream.SendNext(Score);
                stream.SendNext(Round);
            }
            else
            {
                // 非為玩家本人的狀態, 單純接收更新的資料
                 this.Score = (int)stream.ReceiveNext();
                 this.Round = (int)stream.ReceiveNext();
            }
        }
    }
}
