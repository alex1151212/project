﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class scoreBoard : MonoBehaviour
{
    PhotonView pv ; 
    public GameObject score_text1 ; 
    public GameObject score_text2; 
    // Start is called before the first frame update
    void Start()
    {
        pv = GetComponent<PhotonView>();
    }

    // Update is called once per frame
    void Update()
    {
        score_text1.GetComponent<Text>().text = PhotonView.Find(1001).gameObject.GetComponent<Target>().Score.ToString();

        score_text2.GetComponent<Text>().text = PhotonView.Find(2001).gameObject.GetComponent<Target>().Score.ToString();
    }
}
 