﻿using System.Collections;

using System.Collections.Generic;

using UnityEngine;

using Photon.Pun;



public class Target : MonoBehaviourPunCallbacks, IPunObservable
{

    public GameObject target;
    // public bool enable;

    public int Round;
    public int dart;
    public bool ismyturn;

    public bool Catch;

    public int Score;

    private PhotonView pv;
    private static Trigger trigger;
    public static Arrow arrow;
    // public GameObject[] darts = new GameObject[3];


    // Use this for initialization

    private void Start()
    {
        pv = this.GetComponent<PhotonView>();

        if (photonView.IsMine)
        {
            // Init();
            Score = 0;
            // enable = true;
        }

    }

    // public void Init()
    // {

    //     if (arrow == null)
    //         arrow = Instantiate<Arrow>(Resources.Load<Arrow>("dart"));


    //     Catch = false;
    //     trigger = null;
    //     enable = true;
    //     arrow.Init();
    // }
    public void Init_arrow()
    {

        arrow = null;
        trigger = null;
        Catch = false;
    }


    public void Catched()
    {
        Catch = true;
    }

    public static void SetArrow(Arrow arw)
    {
        arrow = arw;
    }

    public static void SetTrigger(Trigger tri)
    {
        trigger = tri;
    }

    // private void OnGUI()
    // {
    //     if (Score != -1)
    //     {
    //         GUI.Label(new Rect(400, 80, 100, 30), "Your Score is " + Score);

    //         GUI.Label(new Rect(400, 100, 150, 30), "push blank to rebegin");
    //     }

    // }


    void Update()
    {

        if (pv.IsMine)
        {
            // if (Input.GetKeyDown(KeyCode.Space))
            // {
            //     Debug.Log("JUMP");
            //     Init();
            // }
            // if (enable)
            // {
            // if (Catch == true)
            // {
            if (arrow.transform.position.z - 4.37 > -0.05)
            {
                // print(target.GetComponent<Transform>().position.z);

                arrow.GetComponent<Rigidbody>().isKinematic = true;

                arrow.GetComponent<Rigidbody>().useGravity = false;

                // print(arrow.transform.position.z - target.GetComponent<Transform>().position.z);
                // print(trigger.gameObject.name);

                switch (trigger.gameObject.name)
                {
                    case "25":
                        Score +=25;
                        break;

                    case "50":
                        Score +=50 ;
                        break;
                        
                    case "20":
                        if (trigger.gameObject.tag == "triple")
                        {
                            Score += 60;
                        }
                        else if (trigger.gameObject.tag == "double")
                        {
                            Score += 40;
                        }
                        else if (trigger.gameObject.tag == "point")
                        {
                            Score += 20;
                        }

                        break;

                    case "19":
                        if (trigger.gameObject.tag == "triple")
                        {
                            Score += 57;
                        }
                        else if (trigger.gameObject.tag == "double")
                        {
                            Score += 38;
                        }
                        else if (trigger.gameObject.tag == "point")
                        {
                            Score += 19;
                        }

                        break;

                    case "18":
                        if (trigger.gameObject.tag == "triple")
                        {
                            Score += 54;
                        }
                        else if (trigger.gameObject.tag == "double")
                        {
                            Score += 36;
                        }
                        else if (trigger.gameObject.tag == "point")
                        {
                            Score += 18;
                        }

                        break;

                    case "17":
                        if (trigger.gameObject.tag == "triple")
                        {
                            Score += 51;
                        }
                        else if (trigger.gameObject.tag == "double")
                        {
                            Score += 34;
                        }
                        else if (trigger.gameObject.tag == "point")
                        {
                            Score += 17;
                        }

                        break;

                    case "16":
                        if (trigger.gameObject.tag == "triple")
                        {
                            Score += 48;
                        }
                        else if (trigger.gameObject.tag == "double")
                        {
                            Score += 32;
                        }
                        else if (trigger.gameObject.tag == "point")
                        {
                            Score += 16;
                        }

                        break;

                    case "15":
                        if (trigger.gameObject.tag == "triple")
                        {
                            Score += 45;
                        }
                        else if (trigger.gameObject.tag == "double")
                        {
                            Score += 30;
                        }
                        else if (trigger.gameObject.tag == "point")
                        {
                            Score += 15;
                        }

                        break;

                    case "14":
                        if (trigger.gameObject.tag == "triple")
                        {
                            Score += 42;
                        }
                        else if (trigger.gameObject.tag == "double")
                        {
                            Score += 28;
                        }
                        else if (trigger.gameObject.tag == "point")
                        {
                            Score += 14;
                        }

                        break;

                    case "13":
                        if (trigger.gameObject.tag == "triple")
                        {
                            Score += 39;
                        }
                        else if (trigger.gameObject.tag == "double")
                        {
                            Score += 26;
                        }
                        else if (trigger.gameObject.tag == "point")
                        {
                            Score += 13;
                        }

                        break;

                    case "12":
                        if (trigger.gameObject.tag == "triple")
                        {
                            Score += 36;
                        }
                        else if (trigger.gameObject.tag == "double")
                        {
                            Score += 24;
                        }
                        else if (trigger.gameObject.tag == "point")
                        {
                            Score += 12;
                        }

                        break;

                    case "11":
                        if (trigger.gameObject.tag == "triple")
                        {
                            Score += 33;
                        }
                        else if (trigger.gameObject.tag == "double")
                        {
                            Score += 22;
                        }
                        else if (trigger.gameObject.tag == "point")
                        {
                            Score += 11;
                        }

                        break;

                    case "10":
                        if (trigger.gameObject.tag == "triple")
                        {
                            Score += 30;
                        }
                        else if (trigger.gameObject.tag == "double")
                        {
                            Score += 20;
                        }
                        else if (trigger.gameObject.tag == "point")
                        {
                            Score += 10;
                        }

                        break;

                    case "9":
                        if (trigger.gameObject.tag == "triple")
                        {
                            Score += 27;
                        }
                        else if (trigger.gameObject.tag == "double")
                        {
                            Score += 18;
                        }
                        else if (trigger.gameObject.tag == "point")
                        {
                            Score += 9;
                        }

                        break;

                    case "8":
                        if (trigger.gameObject.tag == "triple")
                        {
                            Score += 24;
                        }
                        else if (trigger.gameObject.tag == "double")
                        {
                            Score += 16;
                        }
                        else if (trigger.gameObject.tag == "point")
                        {
                            Score += 8;
                        }

                        break;

                    case "7":
                        if (trigger.gameObject.tag == "triple")
                        {
                            Score += 21;
                        }
                        else if (trigger.gameObject.tag == "double")
                        {
                            Score += 14;
                        }
                        else if (trigger.gameObject.tag == "point")
                        {
                            Score += 7;
                        }

                        break;

                    case "6":
                        if (trigger.gameObject.tag == "triple")
                        {
                            Score += 18;
                        }
                        else if (trigger.gameObject.tag == "double")
                        {
                            Score += 12;
                        }
                        else if (trigger.gameObject.tag == "point")
                        {
                            Score += 6;
                        }

                        break;

                    case "5":
                        if (trigger.gameObject.tag == "triple")
                        {
                            Score += 15;
                        }
                        else if (trigger.gameObject.tag == "double")
                        {
                            Score += 10;
                        }
                        else if (trigger.gameObject.tag == "point")
                        {
                            Score += 5;
                        }

                        break;

                    case "4":
                        if (trigger.gameObject.tag == "triple")
                        {
                            Score += 12;
                        }
                        else if (trigger.gameObject.tag == "double")
                        {
                            Score += 8;
                        }
                        else if (trigger.gameObject.tag == "point")
                        {
                            Score += 4;
                        }

                        break;

                    case "3":
                        if (trigger.gameObject.tag == "triple")
                        {
                            Score += 9;
                        }
                        else if (trigger.gameObject.tag == "double")
                        {
                            Score += 6;
                        }
                        else if (trigger.gameObject.tag == "point")
                        {
                            Score += 3;
                        }

                        break;

                    case "2":
                        if (trigger.gameObject.tag == "triple")
                        {
                            Score += 6;
                        }
                        else if (trigger.gameObject.tag == "double")
                        {
                            Score += 4;
                        }
                        else if (trigger.gameObject.tag == "point")
                        {
                            Score += 2;
                        }

                        break;

                    case "1":
                        if (trigger.gameObject.tag == "triple")
                        {
                            Score += 3;
                        }
                        else if (trigger.gameObject.tag == "double")
                        {
                            Score += 2;
                        }
                        else if (trigger.gameObject.tag == "point")
                        {
                            Score += 1;
                        }

                        break;


                }

                Debug.Log(Score);

                Init_arrow();

                // }
                // }
            }


        }


        // else if(arrow.transform.position.z > 1)
        // {
        //     Score = 0;
        //     Catch = true;
        //     Init();
        // }


    }

    public void OnPhotonSerializeView(PhotonStream stream,
    PhotonMessageInfo info)
    {
        if(stream.IsWriting)
        {
            // 為玩家本人的狀態, 將 IsFiring 的狀態更新給其他玩家
            stream.SendNext(Score);
        
            // 非為玩家本人的狀態, 單純接收更新的資料
            // this.Score = (int)stream.ReceiveNext();
        }
        else
        {
            this.Score = (int)stream.ReceiveNext();
        }
        
    }
}
